#!/usr/bin/perl


use constant TAB => "    ";

sub uniq {
    my %seen;
    return grep { !$seen{$_}++ } @_;
}

sub trim {
    my $s = shift;
    $s =~ s/^\s+|\s+$//g;
    return $s
}

sub loadDependency {
    ( $src ) = @_;
    open(DATA, "<$src") or die "Couldn't open file '$src', $!";
    @lines = <DATA>;
    close DATA;
    return trim join('', @lines);
}



%aggregates = ();
open(DATA, "<aggregates.properties") or die "Couldn't open file, $!";
while (<DATA>) {
    ($artifact, $name, $bom) = map { trim $_ } split('[=,]', $_, 3);
    $aggregates{$artifact} = {'name' => $name, 'dep' => $bom };
}
close(DATA);


%forcedVersions = {};
open(DATA, "<versions.properties") or die "Couldn't open file, $!";
while (<DATA>) {
    ($name, $version) = map { trim $_ } split('[=]', $_, 2);
    $forcedVersions{"$name.version"} = $version;
}
close(DATA);



@modules = ();

@propertiesOrder = ();
%properties = {};

@dependenciesOrder = ();
%dependencies = {};


while (<>) {
    ($groupId, $artifactId, $type, $version, @row) = split(';', $_);
    if (@modules == 0) {
        @modules = map { trim $_ } @row;
    } else {
        $artifact = "$groupId:$artifactId";

        if ($aggregates{$artifact}) {
            $propertyName = "$aggregates{$artifact}{name}.version";
            $dependencyKey = $aggregates{$artifact}{'dep'};
            if ($dependencyKey) {
                $dependency = loadDependency($dependencyKey);
            } else {
                $dependencyKey = "$groupId:$artifactId";
                $dependency = "<dependency>\n    <groupId>$groupId</groupId>\n    <artifactId>$artifactId</artifactId>\n    <version>\${$propertyName}</version>\n</dependency>"
            }
        } else {
            $propertyName = "$artifactId.version";
            $dependencyKey = "$groupId:$artifactId";
            $dependency = "<dependency>\n    <groupId>$groupId</groupId>\n    <artifactId>$artifactId</artifactId>\n    <version>\${$propertyName}</version>\n</dependency>"
        }

        $forcedVersions{$propertyName} = $version;

        if (!$dependencies{$dependencyKey}) {
            $dependencies{$dependencyKey} = $dependency;
            push @dependenciesOrder, $dependencyKey;
        }

        if (!$properties{$propertyName}) {
            $properties{$propertyName} = { 'deps' => [], 'versions' => {} };
            push @propertiesOrder, $propertyName;
        }

        for ($idx = 2; $idx < (scalar @row); $idx ++) {
            if ($row[$idx] != '') {
                $version = trim($row[$idx]);
                if (!$properties{$propertyName}{'versions'}{$version}) {
                    $properties{$propertyName}{'versions'}{$version} = [];
                }
                push(@{ $properties{$propertyName}{'versions'}{$version} }, $modules[$idx]);
            }
        }

        push(@{ $properties{$propertyName}{'deps'} }, $artifact);
    }
}

print "<!-- 
 Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)

 This software is a computer program whose purpose is to maintain and
 administrate standalone forms.

 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 \"http://www.cecill.info\".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 -->
<project xmlns=\"http://maven.apache.org/POM/4.0.0\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
        xsi:schemaLocation=\"http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd\">

    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>fr.ge.core</groupId>
        <artifactId>ge-pom</artifactId>
        <version>2.3.9.0-SNAPSHOT</version>
        <relativePath>../..</relativePath>
    </parent>

    <artifactId>java-pom</artifactId>
    <packaging>pom</packaging>

    <name>Java POM</name>

";

print TAB, "<properties>\n";
foreach $key ( sort @propertiesOrder ) {
    @versions = keys( %{ $properties{$key}{'versions'} } );

    if (scalar @versions == 0) {
        next;
    }

    print "\n";
    print TAB x 2, "<!--\n";
    print TAB x 3, "dependency(ies) :\n";
    foreach $deps ( @{ $properties{$key}{'deps'} }) {
        print TAB x 4, "- $deps\n";
    }
    print "\n";
    print TAB x 3, "version(s) :\n";
    foreach $version (@versions) {
        print TAB x 4, "- $version : ", join(', ', uniq(@{ $properties{$key}{'versions'}{$version} })), "\n";
    }
    print TAB x 2, " -->\n";
    $version = $forcedVersions{$key};
    if (!$version) {
        $version = $versions[0];
    }
    print TAB x 2, "<$key>$version</$key>\n";
}
print "\n";
print TAB, "</properties>\n";
print "\n";
print TAB, "<dependencyManagement>\n";
print TAB x 2, "<dependencies>\n";
foreach $dependencyKey ( sort @dependenciesOrder) {
    $dependency = $dependencies{$dependencyKey};
    $dependency =~ s/\n/\n            /g ;
    print TAB x 3, "$dependency\n";
}
print TAB x 2, "</dependencies>\n";
print TAB, "</dependencyManagement>\n";

print "
</project>
";
